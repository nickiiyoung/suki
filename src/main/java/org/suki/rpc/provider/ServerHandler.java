package org.suki.rpc.provider;


import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.suki.rpc.model.SukiRequest;
import org.suki.rpc.model.SukiResponse;

import java.lang.reflect.Method;

/**
 * @author ganchaoyang
 * @date 2018/10/29 19:12
 * @description
 */
public class ServerHandler extends SimpleChannelInboundHandler<SukiRequest> {

    private static final Logger logger = LoggerFactory.getLogger(ServerHandler.class);

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("netty provider caught error,", cause);
        ctx.close();
    }

    private Object handle(SukiRequest request) throws Exception {
        String serviceName = request.getServiceName();
        Object o = BeanFactory.getBean(serviceName);
        Class<?> clz = o.getClass();
        // 获取调用的方法名称。
        String methodName = request.getMethodName();
        // 参数类型
        Class<?>[] paramsTypes = request.getParamTypes();
        // 具体参数。
        Object[] params = request.getParams();
        // 调用实现类的指定的方法并返回结果。
        Method method = clz.getMethod(methodName, paramsTypes);
        return method.invoke(o, params);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, SukiRequest sukiRequest) throws Exception {
        logger.info("provider accept request,{}", sukiRequest);
        // 返回的对象。
        SukiResponse sukiResponse=new SukiResponse();
        sukiResponse.setRequestId(sukiRequest.getRequestId());
        try {
            Object result = handle(sukiRequest);
            sukiResponse.setResult(result);
        } catch (Exception e) {
            sukiResponse.setError(e);
        }
        channelHandlerContext.writeAndFlush(sukiResponse).addListener(ChannelFutureListener.CLOSE);
    }
}

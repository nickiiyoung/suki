package org.suki.rpc.provider;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author ganchaoyang
 * @date 2018/10/30 14:39
 * @description
 */
public class BeanFactory {

    /**
     * 持有当前Rpc服务提供的所有的服务
     * 即有@RpcService注解的类
     */
    private static final Map<String, Object> BEANS = new HashMap<>();

    private BeanFactory() {
    }

    public static void addBean(String beanName, Object bean) {
        BEANS.put(beanName, bean);
    }

    public static Object getBean(String beanName) {
        return BEANS.getOrDefault(beanName,null);
    }

    public static Set<String> getBeans() {
        return BEANS.keySet();
    }
}

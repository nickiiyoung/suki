package org.suki.rpc.conf;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.suki.rpc.consumer.SukiProxy;
import org.suki.rpc.exception.ConnectException;
import org.suki.rpc.registry.ServiceDiscovery;


/**
 * rpc汽车配置
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Configuration
@EnableConfigurationProperties(RpcConfProperties.class)
public class RpcAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(RpcAutoConfiguration.class);

    /**
     * rpc配置属性
     */
    @Autowired
    private RpcConfProperties rpcConfProperties;

    @Bean
    public ServiceDiscovery serviceDiscovery() {
        ServiceDiscovery serviceDiscovery = null;
        try {
            serviceDiscovery = new ServiceDiscovery(rpcConfProperties.getRegisterAddress());
        } catch (ConnectException e) {
            logger.error(" connect failed:", e);
        }
        return serviceDiscovery;
    }

    /**
     * rpc代理
     *
     * @return {@link SukiProxy}
     */
    @Bean
    @ConditionalOnMissingBean
    public SukiProxy rpcProxy() {
        SukiProxy rpcProxy = new SukiProxy();
        rpcProxy.setServiceDiscovery(serviceDiscovery());
        return rpcProxy;
    }

}

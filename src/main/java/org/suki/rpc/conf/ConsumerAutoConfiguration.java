package org.suki.rpc.conf;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.suki.rpc.anno.Ref;
import org.suki.rpc.consumer.SukiProxy;

import java.lang.reflect.Field;


/**
 * 消费者汽车配置
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Configuration
@ConditionalOnClass(Ref.class)
@EnableConfigurationProperties(RpcConfProperties.class)
public class ConsumerAutoConfiguration {

    /**
     * rpc代理
     */
    @Autowired
    private SukiProxy rpcProxy;


    /**
     *
     * 设置动态代理
     *
     * @return {@link BeanPostProcessor}
     */
    @Bean
    public BeanPostProcessor beanPostProcessor() {
        return new BeanPostProcessor() {
            @Override
            public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                Class<?> objClz = bean.getClass();
                for (Field field : objClz.getDeclaredFields()) {
                    Ref ref = field.getAnnotation(Ref.class);
                    if (null != ref) {
                        Class<?> type = field.getType();
                        field.setAccessible(true);
                        try {
                            field.set(bean, rpcProxy.create(type, ref.serviceName()));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } finally {
                            field.setAccessible(false);
                        }
                    }
                }
                return bean;
            }
        };
    }

}

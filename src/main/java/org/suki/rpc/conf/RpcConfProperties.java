package org.suki.rpc.conf;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * rpc配置属性
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@ConfigurationProperties(prefix = "suki.rpc")
@Component
@Data
public class RpcConfProperties {
    /**
     * 注册中心地址
     */
    private String registerAddress = "127.0.0.1:8848";

    /**
     * rpc服务的端口
     */
    private int port = 21810;

    /**
     * 命名空间
     */
    private String nameSpace = "default";

    /**
     * 服务地址
     */
    private String host = "localhost";

    /**
     * 超时时间
     */
    private int timeout = 2000;
}

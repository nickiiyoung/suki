package org.suki.rpc.conf;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.suki.rpc.anno.Registry;
import org.suki.rpc.common.SukiDecoder;
import org.suki.rpc.common.SukiEncoder;
import org.suki.rpc.model.SukiRequest;
import org.suki.rpc.model.SukiResponse;
import org.suki.rpc.provider.BeanFactory;
import org.suki.rpc.provider.ServerHandler;
import org.suki.rpc.registry.RegistryServer;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Set;


/**
 * 提供汽车配置
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Configuration
@ConditionalOnClass(Registry.class)
public class ProviderAutoConfiguration {

    private final Logger logger = LoggerFactory.getLogger(ProviderAutoConfiguration.class);

    /**
     * 应用程序上下文
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * rpc配置属性
     */
    @Autowired
    private RpcConfProperties rpcConfProperties;

    /**
     * 初始化
     *
     */
    @PostConstruct
    public void init() {
        logger.info("rpc server start scanning provider service...");
        Map<String, Object> beanMap = this.applicationContext.getBeansWithAnnotation(Registry.class);
        if (!beanMap.isEmpty()) {
            beanMap.forEach(this::initProviderBean);
        }
        logger.info("rpc server scan over...");
        // 如果有服务的话才启动netty server
        if (!beanMap.isEmpty()) {
            startNetty(rpcConfProperties.getPort());
        }
    }

    /**
     * 将服务类交由BeanFactory管理
     *
     * @param beanName
     * @param bean
     */
    private void initProviderBean(String beanName, Object bean) {
        Registry rpcService = this.applicationContext.findAnnotationOnBean(beanName, Registry.class);
        assert rpcService != null;
        BeanFactory.addBean(rpcService.serviceName(), bean);
    }

    /**
     * 启动netty server
     *
     * @param port netty启动的端口
     */
    public void startNetty(int port) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new SukiDecoder(SukiRequest.class))
                                    .addLast(new SukiEncoder(SukiResponse.class))
                                    .addLast(new ServerHandler());
                        }
                    }).option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            ChannelFuture f = b.bind(port).sync();
            logger.info("server started on port : {}", port);
            RegistryServer registryServer = new RegistryServer(rpcConfProperties.getRegisterAddress(), rpcConfProperties.getTimeout(), rpcConfProperties.getNameSpace(),rpcConfProperties.getHost(), port);
            Set<String> beans = BeanFactory.getBeans();
            for (String s:beans){
                registryServer.register(s);
            }
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

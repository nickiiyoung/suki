package org.suki.rpc.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注入
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Ref {
    /**
     * 获取服务名
     */
    String serviceName();
}

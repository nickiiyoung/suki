package org.suki.rpc.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注册表
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Registry {
    /**
     * 获取class
     */
    String serviceName();
}

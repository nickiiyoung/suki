package org.suki.rpc.exception;

/**
 * 连接异常
 *
 * @author zhangmeiyang
 * @description
 * @date 2022/11/07
 */
public class ConnectException extends Exception {

    public ConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}

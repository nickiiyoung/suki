package org.suki.rpc.exception;

/**
 * 处理消息异常
 *
 * @author zhangmeiyang
 * @description
 * @date 2022/11/07
 */
public class HandleMessageException extends Exception {

    public HandleMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}

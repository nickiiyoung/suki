package org.suki.rpc.common;


/**
 * 常量
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
public class Constants {

    /**
     * 服务注册时的父节点
     */
    public static final String ZK_ROOT_DIR = "/rpc";

}

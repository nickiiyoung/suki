package org.suki.rpc.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.Data;
import org.springframework.util.SerializationUtils;

import java.util.List;

/**
 * 译码器
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
public class SukiDecoder extends ByteToMessageDecoder {
    private Class<?> decoderClass;
    private static final int HEAD_LENGTH=4;

    public SukiDecoder (Class<?> decoderClass){
        this.decoderClass=decoderClass;
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        if(byteBuf.readableBytes()<HEAD_LENGTH){
            return;
        }
        //这个HEAD_LENGTH是我们用于表示头长度的字节数。  由于上面我们传的是一个int类型的值，所以这里HEAD_LENGTH的值为4.
        byteBuf.markReaderIndex();
        //标记一下当前的readIndex的位置
        int dataLength = byteBuf.readInt();
        //读取传送过来的消息的长度.ByteBuf的readInt()方法会让他的readIndex增加4
        //data长度
        if (dataLength < 0) {
            channelHandlerContext.close();
        }
        //我们读到的消息体长度为0，这是不应该出现的情况，这里出现这情况，关闭连接。
        if (byteBuf.readableBytes() < dataLength) {
            byteBuf.resetReaderIndex();
            //读到的消息体长度如果小于我们传送过来的消息长度，则resetReaderIndex. 这个配合markReaderIndex使用的。把readIndex重置到mark的地方
        }
        //将ByteBuf转换为byte[]
        byte[] data = new byte[dataLength];
        byteBuf.readBytes(data);
        //将data转换成object
        Object obj = SerializationUtils.deserialize(data);
        //将byte数据转化为我们需要的对象。伪代码，用什么序列化，自行选择
        list.add(obj);
    }
}

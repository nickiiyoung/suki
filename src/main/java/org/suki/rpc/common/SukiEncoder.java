package org.suki.rpc.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.springframework.util.SerializationUtils;

/**
 * 编码器
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
public class SukiEncoder extends MessageToByteEncoder {
    private Class<?> genericClass;

    public SukiEncoder(Class<?> genericClass) {
        this.genericClass = genericClass;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        //序列化
        if (genericClass.isInstance(o)) {
            //使用spring提供的序列化工具类序列化
            byte[] data = SerializationUtils.serialize(o);
            assert data !=null;
            byteBuf.writeInt(data.length);
            byteBuf.writeBytes(data);
        }
    }
}

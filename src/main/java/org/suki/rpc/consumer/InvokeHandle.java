package org.suki.rpc.consumer;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.suki.rpc.model.SukiProvider;
import org.suki.rpc.model.SukiRequest;
import org.suki.rpc.model.SukiResponse;
import org.suki.rpc.registry.ServiceDiscovery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * 调用处理
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
public class InvokeHandle implements InvocationHandler {

    /**
     * 供应商名字
     */
    private String providerName;

    /**
     * 服务发现
     */
    private ServiceDiscovery serviceDiscovery;

    /**
     * 调用
     *
     * @param proxy  代理
     * @param method 方法
     * @param args   arg游戏
     * @return {@link Object}
     * @throws Throwable throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        SukiRequest request = new SukiRequest();
        request.setRequestId(UUID.randomUUID().toString());
        request.setServiceName(providerName);
        request.setMethodName(method.getName());
        request.setParamTypes(method.getParameterTypes());
        request.setParams(args);
        SukiProvider providerInfo = serviceDiscovery.discover(providerName);
        SukiClient rpcClient = new SukiClient(providerInfo.getPort(), providerInfo.getHost());
        SukiResponse response = rpcClient.send(request);
        if (null!=response.getError()) {
            throw response.getError();
        } else {
            return response.getResult();
        }
    }
}

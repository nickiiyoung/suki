package org.suki.rpc.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.suki.rpc.model.SukiProvider;
import org.suki.rpc.model.SukiRequest;
import org.suki.rpc.model.SukiResponse;
import org.suki.rpc.registry.ServiceDiscovery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * 代理
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Component
public class SukiProxy {
    /**
     * 服务发现
     */
    @Autowired
    private ServiceDiscovery serviceDiscovery;

    /**
     * 创建
     *
     * @param clz          clz
     * @param providerName 供应商名字
     * @return {@link T}
     */
    @SuppressWarnings("unchecked")
    public <T> T create(Class<?> clz, String providerName) {
        InvokeHandle invokeHandle = new InvokeHandle();
        invokeHandle.setProviderName(providerName);
        invokeHandle.setServiceDiscovery(serviceDiscovery);
        return (T) Proxy.newProxyInstance(clz.getClassLoader(),new Class<?>[]{clz},invokeHandle);
    }

    public void setServiceDiscovery(ServiceDiscovery serviceDiscovery) {
        this.serviceDiscovery = serviceDiscovery;
    }
}

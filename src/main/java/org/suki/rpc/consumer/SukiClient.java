package org.suki.rpc.consumer;



import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.suki.rpc.common.SukiDecoder;
import org.suki.rpc.common.SukiEncoder;
import org.suki.rpc.model.SukiRequest;
import org.suki.rpc.model.SukiResponse;

import java.util.concurrent.CompletableFuture;

/**
 * 苏琪客户
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SukiClient extends SimpleChannelInboundHandler<SukiResponse> {

    private Logger logger = LoggerFactory.getLogger(SukiClient.class);
    /**
     * 主机
     */
    private String host;

    /**
     * 端口
     */
    private int port;

    /**
     * 异步调用
     */
    private CompletableFuture<String> future;

    /**
     * 返回信息
     */
    private SukiResponse response;

    public SukiClient(int port,String host){
        this.host=host;
        this.port=port;
    }
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, SukiResponse sukiResponse) throws Exception {
        logger.info("client get request result,{}", sukiResponse);
        this.response = sukiResponse;
        future.complete("client has been received the response");
    }

    public SukiResponse send(SukiRequest sukiRequest){
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup).channel(NioSocketChannel.class).
                    handler(new ChannelInitializer<SocketChannel>() {
                        //启动netty new channel进行处理
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new SukiEncoder(SukiRequest.class))
                                    .addLast(new SukiDecoder(SukiResponse.class))
                                    .addLast(SukiClient.this);
                        }//初始化channel添加工作人员
                    }).option(ChannelOption.SO_KEEPALIVE, true);
            // 使用Nio模型添加在队列中添加req resp client，保持连接
            ChannelFuture channelFuture = bootstrap.connect(host, port).sync();
            //连接端口、主机
            channelFuture.channel().writeAndFlush(sukiRequest).sync();
            future = new CompletableFuture<>();
            future.get();
            if (response != null) {
                // 关闭netty连接。
                channelFuture.channel().closeFuture().sync();
            }
            return response;
        }catch (Exception e){
            logger.error("client send msg error,", e);
            return null;
        }finally {
            workerGroup.shutdownGracefully();//关闭工作流
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("netty client caught exception,", cause);
        ctx.close();
    }
}

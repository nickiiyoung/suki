package org.suki.rpc.registry;


import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.suki.rpc.exception.ConnectException;
import org.suki.rpc.model.SukiProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 服务发现
 *
 * @author zhangmeiyang
 * @description
 * @date 2022/11/07
 */
public class ServiceDiscovery {

    private NamingService naming;

    /**
     * 服务发现
     *
     * @param registryAddress 注册地址
     * @throws ConnectException 连接异常
     */
    public ServiceDiscovery(String registryAddress) throws ConnectException {
        try {
            this.naming = NamingFactory.createNamingService(registryAddress);
        } catch (Exception e) {
            throw new ConnectException("connect fail,cause" + e.getMessage(), e.getCause());
        }
    }


    /**
     * 服务发现
     *
     * @param providerName 供应商名字
     * @return {@link SukiProvider}
     * @throws NacosException 纳科例外
     */
    public SukiProvider discover(String providerName) throws NacosException {
        List<Instance> allInstances = this.naming.getAllInstances(providerName);
        List<SukiProvider> providerInfos =new ArrayList<>();
        if (allInstances.isEmpty()){
            return null;
        }
        SukiProvider sukiProvider = null;
        for (Instance instance:allInstances){
            sukiProvider=new SukiProvider();
            sukiProvider.setPort(instance.getPort());
            sukiProvider.setHost(instance.getIp());
            sukiProvider.setName(instance.getInstanceId());
            providerInfos.add(sukiProvider);
        }
        return providerInfos.get(ThreadLocalRandom.current().nextInt(providerInfos.size()));
        //获取服务提供者
    }
}

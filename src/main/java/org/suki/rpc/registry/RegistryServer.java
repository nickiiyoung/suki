package org.suki.rpc.registry;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.suki.rpc.exception.ConnectException;

/**
 * 注册中心服务器
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
public class RegistryServer {
    private Logger logger = LoggerFactory.getLogger(RegistryServer.class);

    private String address;

    private int timeout;

    private String nameSpace;

    private String host;

    private int port;

    public RegistryServer(String address, int timeout, String nameSpace,String host, int port) throws NacosException {
        this.address = address;
        this.timeout = timeout;
        this.nameSpace = nameSpace;
        this.host = host;
        this.port = port;
    }

    /**
     * 注册
     *
     * @param serverName 服务器名称
     * @throws ConnectException 连接异常
     */
    public void register(String serverName) throws ConnectException {
        try {
            NamingService naming = NamingFactory.createNamingService(this.address);
            Instance instance = new Instance();
            instance.setIp(this.host);
            instance.setPort(this.port);
            instance.setHealthy(true);
            instance.setWeight(2.0);
            instance.setInstanceId(serverName);
            naming.registerInstance(serverName,instance);
        } catch (Exception e) {
            throw new ConnectException("register fail ,cause" + e.getMessage(), e.getCause());
        }
    }
}

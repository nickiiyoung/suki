package org.suki.rpc.model;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 苏琪请求
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class SukiRequest implements Serializable {

    /**
     * 请求id
     */
    private String requestId;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 参数类型
     */
    private Class<?>[] paramTypes;

    /**
     *params
     */
    private Object[] params;
}

package org.suki.rpc.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 苏琪响应
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class SukiResponse implements Serializable {

    /**
     * 请求id
     */
    private String requestId;

    /**
     * 错误
     */
    private Throwable error;

    /**
     * 结果
     */
    private Object result;
}

package org.suki.rpc.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 苏琪提供者
 *
 * @author zhangmeiyang
 * @date 2022/11/07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SukiProvider implements Serializable {

    /**
     * 名字
     */
    private String name;

    /**
     * 宿主
     */
    private String host;

    /**
     * 港口
     */
    private int port;
}

package org.suki;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;

public class AppcodeTest {
    public static void main(String[] args) throws NacosException {
        while (true){
            NamingService naming = NamingFactory.createNamingService("127.0.0.1:8848");
            Instance instance = new Instance();
            instance.setIp("127.0.0.1");
            instance.setPort(21080);
            instance.setHealthy(false);
            instance.setWeight(2.0);
            naming.registerInstance("trainService",instance);

            Instance instance1 = new Instance();
            instance.setIp("127.0.0.1");
            instance.setPort(21080);
            instance.setHealthy(false);
            instance.setWeight(2.0);
            naming.registerInstance("trainService1",instance1);
        }
    }
}
